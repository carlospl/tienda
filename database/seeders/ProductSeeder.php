<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();
        DB::table('products')->insert([
            'nombre' => 'product_name',
            'precio' => 12.5,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/índice.jpeg')),
        ]);
        DB::table('products')->insert([
            'nombre' => 'Pinguino',
            'precio' => 20,
            'stock' => 8,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/índice.jpeg')),
        ]);
        DB::table('products')->insert([
            'nombre' => 'Pato',
            'precio' => 8,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/índice.jpeg')),
        ]);
        DB::table('products')->insert([
            'nombre' => 'product_name',
            'precio' => 12.5,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/índice.jpeg')),
        ]);
        //
    }
}
