

@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card ">
                    <div class="card-header">{{ __('Products') }}
                            <form method="get" action="/products">
                                <input name="price" type="text" placeholder="Productos mas caros que"/>
                            <span>Minimo:</span>
                            <input name="minimo" type="text" placeholder="0"/>
                            <span>Maximo:</span>
                            <input name="maximo" type="text" placeholder="999"/>
                            <input value="Busca" type="submit"/>
                        </form>
                    </div>
                    <div class="card-body col-8 text-center">
                        @foreach ($products as $product)
                            <img class="card-img-top"src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                            <li>{{$product->nombre}}</li>
                            <li>{{$product->precio}}</li>
                            <li>{{$product->descripcion}}</li>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
                            <br>
                        @endforeach
                        {{--{{$products->appends['minimo'=>12]}}--}}

                            {{$products->appends(['maximo'=>empty($_GET['maximo'])?999:$_GET['maximo'],'minimo'=>empty($_GET['minimo'])?1:$_GET['minimo']])->links()}}
                    </div>
                </div> </div> </div> </div>@endsection



