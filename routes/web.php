<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Controllers\ProductController;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('lang/{lang}',function($lang){
   session(['lang'=>$lang]);
   App::setlocale($lang);
   return redirect()->back();
});

Route::get('/products', [ProductController::class,'show'])->name('products');
//Route::get('/products', [ProductController::class,' showFiltraje'])->name('products');

Route::get('add-to-cart/{id}', [ProductController::class,'addToCart']);

Route::get('cart', [ProductController::class,'cart'])->name('cart');

Route::patch('update-cart',[ProductController::class,'update']);
Route::delete('remove-from-cart',[ProductController::class,'remove']);

Route::get('venta', [ProductController::class,'venta'])->middleware('venta');
Route::get('emptyCart', [ProductController::class,'emptyCart']);

/*Route::middleware(['auth:sanctum', 'verified'])->get('/products',function(){
$products=App\Models\Product::all();
return view('products')->with('products',$products);
});*/
