<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Product;
use Illuminate\Support\Facades\Log;


class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function prueba()
    {
        $products = DB::table('products')->where('name', 'product_name')->get();
        return view('products')->with('products', $products);
    }

    public function show(Request $request)
    {
        $value = empty($request->input('price')) ? '0' : $request->input('price');

        $minimo = empty($request->input('minimo')) ? '0' : $request->input('minimo');
        $maximo = empty($request->input('maximo')) ? '0' : $request->input('maximo');
        if($value==0){
            $all = DB::select(DB::raw("select * from products where precio > $value ;" ));
        }
        if($maximo==0){
            $all = DB::select(DB::raw("select * from products where precio > $value ;" ));
        }
        if($minimo >=0 && $maximo >0 && $maximo>$minimo){
            $all = DB::select(DB::raw("select * from products where precio  between $minimo and $maximo;" ));
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }/*
    public function showFiltraje(Request $request){

        $minimo = empty($request->input('minimo')) ? '0' : $request->input('minimo');
        $maximo = empty($request->input('maximo')) ? '1000' : $request->input('maximo');

        $all = DB::select(DB::raw("select * from products where precio  between $minimo and $maximo" ));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }
*/


    public function addToCart($id)
    {
        $product = Product::find($id);
        Log::error('error'.$id);
        Log::warning('warning' .$id);
        Log::notice('notice'.$id);
        Log::info('info'.$id);
        Log::debug('debug'.$id);

        Log::channel('buy')->info('Ejemplo log info');
        if (!$product) {
            abort(404);
        }
        $user = Auth::user()->id;
        $cart_bd = Cart::where('user_id', $user)->first();
        if (empty($cart_bd)) {
            $cart_bd = new Cart();
            $cart_bd->user_id = $user;
            $cart_bd->save();
        }
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $id => [
                    "nombre" => $product->nombre,
                    "quantity" => 1,
                    "precio" => $product->precio,
                    "image" => $product->image
                ]
            ];

            $cart_item = new Cart_Item();
            $cart_item->cart_id = $cart_bd->id;
            $cart_item->product_id = $id;
            $cart_item->quantity = 1;
            $cart_item->save();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if (isset($cart[$id])) {
            $cart[$id]['quantity']++;
            $cart_item = Cart_Item::where([['cart_id', $cart_bd->id], ['product_id', $id]])->first();
            $cart_item->quantity = $cart[$id]['quantity'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "nombre" => $product->nombre,
            "quantity" => 1,
            "precio" => $product->precio,
            "image" => $product->image
        ];
        $cart_item = new Cart_Item();
        $cart_item->cart_id = $cart_bd->id;
        $cart_item->product_id = $id;
        $cart_item->quantity = 1;
        $cart_item->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        $user = Auth::user()->id;
        $cart = session()->get('cart');
        $cart_db = Cart::where('user_id', $user)->first();
        $cart_item=Cart_Item::all()->where('cart_id',$cart_db->id);
        foreach($cart_item as $item) {
            $product_id = $item->product_id;
            $product = Product::find($product_id);

            $cart[$product_id] = [
                "nombre" => $product->nombre,
                "quantity" => $item->quantity,
                "precio" => $product->precio,
                "image" => $product->image

            ];
        }
        session()->put('cart',$cart);
        /*if ($cartbd) {
            $cart = session()->get('cart');
            $cart_item = Cart_Item::where('cart_id', $cartbd->id)->get();
            foreach ($cart_item as $item => $details) {

                if (!$cart) {
                    $cart = [
                        $user => [
                            "nombre" => $details->nombre,
                            "quantity" => $details->quantity,
                            "precio" => $details->precio,
                            "image" => $details->image
                        ]
                    ];
                    session()->put('cart', $cart);
                }
            }

        }*/
        return view('cart');
        }
    public function emptyCart()
    {

        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        $id = $cart_db->id;
        $cart = [

        ];
        unset($cart[$id]);
        session()->put('cart', $cart);

        DB::select(DB::raw("delete from cart_items where cart_id=$id"));
        return redirect()->back()->with('success', 'Cart eliminated successfully!');

    }

    public function update(Request $request)
    {
        if ($request->id && $request->quantity) {
            $cart = session()->get('cart');
            $cart[$request->id]["quantity"] = $request->quantity;
            $user = Auth::user()->id;
            $cart_bd = Cart::where('user_id', $user)->first();
            $cart_item = Cart_Item::where([['cart_id', $cart_bd->id], ['product_id', $request->id]])->first();
            $cart_item->quantity = $request->quantity;
            $cart_item->update();

            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_bd = Cart::where('user_id', $user)->first();
                $cart_item = Cart_Item::where([['cart_id', $cart_bd->id], ['product_id', $request->id]])->first();
                $cart_item->delete();
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta()
    {
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        $id = $cart_db->id;
        //$productId = DB::select(DB::raw("update stock from products where id=$prodId"));


        $products = DB::select(DB::raw("select * from cart_items where cart_id=$id"));
        for ($i=0;$i<count($products);$i++){
            $prodId=$products[$i]->product_id;
            $amount=$products[$i]->quantity;
            $p=Product::find($products[$i]->product_id);
            if ($amount > $p->stock){
                return Redirect::back()->withErrors(['Alert: Not enough stock','Alert: Not enough stock']);
            }else{
                DB::update(DB::raw("update products set stock =($p->stock-$amount) where id=$prodId"));
            }

        }
        DB::delete(DB::raw("delete from cart_items where cart_id=$id"));
        session()->remove('cart');
        return redirect()->back()->with('success', 'Congrats, you bought some random shit (: ');

    }
}
